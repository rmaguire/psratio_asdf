#!/bin/bash
for script in `ls ../params_new`; 
do
    IN=$script
    arrIN=(${IN//_/ })
    IN=${arrIN[1]}
    arrIN=(${IN//.dat/})
    dset=`basename ${arrIN}`

    echo 'submitting' ${dset}
    sbatch submit_download.sbatch ${dset}
    sleep 10

done
